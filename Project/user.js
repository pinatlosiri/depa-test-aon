var express = require('express');
var mysql = require('mysql');
var router = express.Router();

var con = mysql.createPool({
    host: 'localhost',
    user: 'usr',
    password: 'usr',
    database: 'test_db',
    connectionLimit: 10,
    multipleStatements: true,
});

router.get('/', function(req, res){
    res.end("get user");
});

router.post('/', function(req, res){
    let user = req.body;
    let sql = `
        insert into user( code, name, pwd)
        values('${user.code}', '${user.name}', '${user.password}' )
    `;
    con.getConnection( function(err, connection) {
        if(err) {
            res.end("error....");
        }else {
            connection.query(sql, function(error, result){
                if(error) {
                    res.end("sql error");
                }else {
                    res.end("insert success");
                }
            });
        }
    });
    
});

router.put('/', function(req, res){
    res.end("update user");
});

router.delete('/', function(req, res){
    res.end("delete user");
});

module.exports = router;
